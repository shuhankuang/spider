#!/bin/bash
#

website='http://w3layouts.com'
agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.104 Safari/537.36"

link=$1

echo $link > link.txt
folder=`cat link.txt | cut -d'/' -f4`
folder='downloads/'$folder


mkdir -p $folder

wget -O t.txt $link --cookies=on --keep-session-cookies --quiet

if [[ -f 't.txt' ]]; then
	#下载图片
	#电脑版
	img=`cat t.txt | grep "web_preview" | cut -d'<' -f4 | cut -d'=' -f2 | cut -d'"' -f2`
	#echo $img
	wget $img -O $folder'/web.jpg'
	#平板
	img=`cat t.txt | grep "smartphopne_preview" | cut -d'<' -f4 | cut -d'=' -f3 | cut -d'"' -f2`
	#echo $img
	wget $img -O $folder'/smartphopne.jpg'
	#平板
	img=`cat t.txt | grep "mobile_preview" | cut -d'<' -f4 | cut -d'=' -f3 | cut -d'"' -f2`
	# echo $img
	wget $img -O $folder'/mobile.jpg'


	#下载 pack zip
	downloadPack=`cat t.txt | grep "Download Pack" | cut -d'>' -f2 | cut -d'?' -f2 | cut -d'"' -f1 | cut -d'<' -f1`
    downloadPack=$website'/?'$downloadPack

    echo "pack-link:"$downloadPack
    wget -O d.txt $downloadPack --cookies=on --keep-session-cookies --quiet

    dl=`cat d.txt | grep "<div class=\"dload-button\" style=\"float:right;\">" | cut -d'"' -f6`
    echo $dl
    
    wget -c -O $folder'/pack.zip' $dl  \
    --referer='$reffer' \
	--cookies=off --header "Cookie: JSESSIONID=$jsessionid" \
	--user-agent='$agent' \
	--progress=bar \

	# 下载 webzip
	downloadWeb=`cat t.txt | grep "Web Version" | cut -d'>' -f2 | cut -d'"' -f2`
	downloadWeb=$website$downloadWeb
	echo $downloadWeb

	echo "web-link:"$downloadWeb
	wget -O w.txt $downloadWeb --cookies=on --keep-session-cookies --quiet

	dl=`cat w.txt | grep "<div class=\"dload-button\" style=\"float:right;\">" | cut -d'"' -f6`
	echo $dl

	wget -c -O $folder'/web.zip' $dl  \
	--referer='$reffer' \
	--cookies=off --header "Cookie: JSESSIONID=$jsessionid" \
	--user-agent='$agent' \
	--progress=bar \

	# rm -rf d.txt
	# rm -rf w.txt
	# rm -rf t.txt
	# rm -rf link.txt
fi