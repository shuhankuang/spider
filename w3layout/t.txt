<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta charset="UTF-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template by w3layouts</title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="http://w3layouts.com/xmlrpc.php" />
<!--[if lt IE 9]>
<script src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/js/html5.js?32543a" type="text/javascript"></script>
<![endif]-->
<link  rel="stylesheet" href="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/css/style-web.css?32543a">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,700,300' rel='stylesheet' type='text/css'>
<link href='http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/fonts/icons.css?32543a' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript">$(document).ready(function(){$(".flexy-menu").flexymenu({speed: 400,type: "horizontal",align: "right"});});</script>
<script type="text/javascript" src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/js/web/flexy-menu.js?32543a"></script>
<script type="text/javascript" src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/js/web/move-top.js?32543a"></script>
<script type="text/javascript" src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/js/web/easing.js?32543a"></script>

<!-- This site is optimized with the Yoast WordPress SEO plugin v1.6.3 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="description" content="Bhagaskara a Multipurpose Flat web template use Bhagaskara to start your agencies and creative studios it&#039;s a responsive html5 css3 web design."/>
<link rel="canonical" href="http://w3layouts.com/bhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template by w3layouts" />
<meta property="og:description" content="Bhagaskara a Multipurpose Flat web template use Bhagaskara to start your agencies and creative studios it&#039;s a responsive html5 css3 web design." />
<meta property="og:url" content="http://w3layouts.com/bhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template/" />
<meta property="og:site_name" content="w3layouts.com" />
<meta property="article:tag" content="Bootstrap" />
<meta property="article:tag" content="Bootstrap Templates" />
<meta property="article:tag" content="Corporate" />
<meta property="article:tag" content="Corporate Business" />
<meta property="article:tag" content="Corporate Portfolio" />
<meta property="article:tag" content="Flat" />
<meta property="article:tag" content="Flat Style" />
<meta property="article:tag" content="Multipurpose" />
<meta property="article:tag" content="singlepage" />
<meta property="article:section" content="Corporate Business" />
<meta property="article:published_time" content="2014-11-06T16:49:47+00:00" />
<meta property="fb:admins" content="800595030" />
<meta property="og:image" content="http://wwwcdn.w3layouts.com/wp-content/uploads/2014/11/Bhagaskara-future.jpg?32543a" />
<meta name="twitter:card" content="summary"/>
<meta name="twitter:site" content="@w3layouts"/>
<meta name="twitter:domain" content="w3layouts.com"/>
<meta name="twitter:creator" content="@w3layouts"/>
<!-- / Yoast WordPress SEO plugin. -->

<link rel="alternate" type="application/rss+xml" title="w3layouts.com &raquo; Feed" href="http://w3layouts.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="w3layouts.com &raquo; Comments Feed" href="http://w3layouts.com/comments/feed/" />
<link rel='stylesheet' id='cntctfrm_stylesheet-css'  href='http://wwwcdn.w3layouts.com/wp-content/plugins/contact-form-plugin/css/style.css?32543a?ver=4.0' type='text/css' media='all' />
<link rel='stylesheet' id='w3layouts-style-css'  href='http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/style.css?32543a?ver=4.0' type='text/css' media='all' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://w3layouts.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://wwwcdn.w3layouts.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.0" />
<link rel='shortlink' href='http://w3layouts.com/?p=8387' />
	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<!-- AdPacks.com Ad Code -->
<script type="text/javascript">
(function(){
  var bsa = document.createElement('script');
     bsa.type = 'text/javascript';
     bsa.async = true;
     bsa.src = '//s3.buysellads.com/ac/bsa.js';
  (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);
})();
</script>
<!-- End AdPacks.com Ad Code -->

</head>

<body>
	<div class="header">
	        <div class="wrap">		
		 		 <div class="logo">
					<a href="http://w3layouts.com/"><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/logo.png?32543a" alt="w3layouts.com Free Mobile website templates Designs" /></a>
				  </div>
				     <div class="search_box">
				         <form action="http://w3layouts.com/" method="get" >
				<input type="text" name="s" class="textbox" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
			 <input type="submit" value=""></form>
				     </div>	
					 <div class="menu">
							<ul class="flexy-menu thick">
								<li><a href="http://w3layouts.com/">Home</a></li>
						<!--<li><a href="http://w3layouts.com/Services">Services</a>
				<ul class="services">
				<li><a href="http://w3layouts.com/Services/psd2html">PSD to HTML</a></li>
				<li><a href="http://w3layouts.com/Services/web2mobile">WEB to Mobile</a></li>
				<li><a href="http://w3layouts.com/Services/psd2wp">PSD to Wordpress</a></li>
			</ul>
			</li>--->
			<li><a href="#">Categories</a>
				<ul class="categories">
							   <div class="submenu">
				            	 <li><a href="http://w3layouts.com/page-not-found/" title="View all posts in 404 Page not Found" >404 Page not Found</a></li><li><a href="http://w3layouts.com/agriculture/" title="View all posts in Agriculture" >Agriculture</a></li><li><a href="http://w3layouts.com/animals-pets/" title="View all posts in Animals &amp; Pets" >Animals &amp; Pets</a></li><li><a href="http://w3layouts.com/autos-transportation/" title="View all posts in Autos &amp; Transportation" >Autos &amp; Transportation</a></li><li><a href="http://w3layouts.com/basic-template-set/" title="View all posts in basic template set" >basic template set</a></li><li><a href="http://w3layouts.com/beauty-and-spa/" title="View all posts in Beauty and Spa" >Beauty and Spa</a></li><li><a href="http://w3layouts.com/blogging-template/" title="View all posts in Blogging Template" >Blogging Template</a></li><li><a href="http://w3layouts.com/corporate-web-templates/" title="View all posts in Corporate Business" >Corporate Business</a></li><li><a href="http://w3layouts.com/ecommerce-online-shopping-mobile-website-templates/" title="View all posts in Ecommerce Online Shopping" >Ecommerce Online Shopping</a></li><li><a href="http://w3layouts.com/education/" title="View all posts in Education School" >Education School</a></li><li><a href="http://w3layouts.com/entertainment/" title="View all posts in Entertainment Music" >Entertainment Music</a></li><li><a href="http://w3layouts.com/hotels-and-restaurants/" title="View all posts in Hotels and Restaurants" >Hotels and Restaurants</a></li> </div><div class="submenu"><li><a href="http://w3layouts.com/industrial-web-and-mobile-templates/" title="View all posts in Industrial" >Industrial</a></li><li><a href="http://w3layouts.com/interior-and-furniture/" title="View all posts in Interior and Furniture" >Interior and Furniture</a></li><li><a href="http://w3layouts.com/medical/" title="View all posts in Medical Hospital" >Medical Hospital</a></li><li><a href="http://w3layouts.com/mobile-application-templates/" title="View all posts in Mobile App Templates" >Mobile App Templates</a></li><li><a href="http://w3layouts.com/mobile-content-portal/" title="View all posts in Mobile Content Portal" >Mobile Content Portal</a></li><li><a href="http://w3layouts.com/music-web-templates/" title="View all posts in Music web Templates" >Music web Templates</a></li><li><a href="http://w3layouts.com/personal/" title="View all posts in Personal website" >Personal website</a></li><li><a href="http://w3layouts.com/gallery-templates/" title="View all posts in PhotoGallery Templates" >PhotoGallery Templates</a></li><li><a href="http://w3layouts.com/builders-and-real-estates/" title="View all posts in Real-estates Builders" >Real-estates Builders</a></li><li><a href="http://w3layouts.com/sports/" title="View all posts in Sports" >Sports</a></li><li><a href="http://w3layouts.com/travel/" title="View all posts in Travel Agency" >Travel Agency</a></li><li><a href="http://w3layouts.com/ui-kits/" title="View all posts in UI Kits" >UI Kits</a></li> </div><div class="submenu"><li><a href="http://w3layouts.com/under-construction-template/" title="View all posts in Under Construction Template" >Under Construction Template</a></li><li><a href="http://w3layouts.com/video-content-portal/" title="View all posts in Video Content Portal" >Video Content Portal</a></li><li><a href="http://w3layouts.com/free-web-elements/" title="View all posts in web Elements" >web Elements</a></li><li><a href="http://w3layouts.com/web-hosting-and-domain-templates/" title="View all posts in Web Hosting Templates" >Web Hosting Templates</a></li><li><a href="http://w3layouts.com/wedding/" title="View all posts in Wedding" >Wedding</a></li><li><a href="http://WPMthemes.com">WordPress Mobile</a></li>
					<li><a href="/logos">Logos</a></li>
					<li><a href="http://w3layouts.com/free-responsive-html5-css3-website-templates/">Responsive Templates</a>
	</li>
	            </div>
			</ul>
			</li>
			<li><a href="http://w3layouts.com/license/">License</a></li>
	      <li><a href="http://w3layouts.com/donate/" >Donate</a></li>
	      <li><a href="http://support.w3layouts.com" >Suppport</a></li>
								<div class="clear"></div>
							</ul>
						</div> 
						<div class="clear"></div>				      
				  </div>							  
	         </div>
	     <div style="margin: 0 auto;text-align: center;margin-top: 5px;"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?32543a"></script>
<!-- w3layouts_main_790x90 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-9153409599391170"
     data-ad-slot="5168336680"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></div><div class="main">         
   <div class="wrap"> 
   	<div class="sub-title"><h1>Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template</h1></div>	
   	<div class="content">
	<div class="Preview-page">

			<link  rel="stylesheet" href="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/fonts/icons.css?32543a">
				
				<div class="section group">
					<div class="web_1_of_3 prev_1_of_3">
					<!--<h3>Desktop Web Templates</h3>-->
					<div class="web">
						<div class="web_preview"><a href="/preview/?l=/bhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template/" target="_blank"><img src="http://wwwcdn.w3layouts.com/wp-content/uploads/2014/11/Bhagaskara-web.jpg?32543a" alt="Free website template CSS HTML5 Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template" /></a></div>
						 <div class="links webe_links">
						  <p><a href="/preview/?l=/bhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template/" title="Demo of CSS HTML5 website template Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template" target="_blank">Demo</a> |
							<a href="/?l=8387&t=web" title="Download web version Free website template CSS HTML5 Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template" rel="nofollow">Download</a>
						  <!---/download/?l=/bhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template/&t=web---></p>
					 </div>						
				  </div>	
				</div>
				<div class="smart_1_of_3 prev_1_of_3">
					<!--<h3>IPhone Mobile Templates</h3>-->
					<div class="smart">
						<div class="smartphopne_preview"><a href="/preview/?d=smartphone&l=/bhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template/" target="_blank"><img alt="Free Iphone Smartphone web template Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template" src="http://wwwcdn.w3layouts.com/wp-content/uploads/2014/11/Bhagaskara-smartphone.jpg?32543a" /></a></div>
						<div class="links smartphone_links">
						<p><a href="/preview/?d=smartphone&l=/bhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template/" target="_blank" title="Demo of Iphone Smartphone web template Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template">Demo</a> |
							<a href="/?l=8387&t=smartphone" title="Download Iphone Smratphone Version web template Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template" rel="nofollow">Download </a>
						</p>
					</div>
					 </div>	
					  
				</div>
				<div class="high_1_of_3 prev_1_of_3">
					  <!--<h3>Highend Mobile Template</h3>-->
					  <div class="mobile">
						<div class="mobile_preview"><a href="/preview/?d=mobile&l=/bhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template/" target="_blank"><img alt="Mobile website Template Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template" src="http://wwwcdn.w3layouts.com/wp-content/uploads/2014/11/Bhagaskara-mobile.jpg?32543a" /></a> </div>
					      <div class="links mobile_links">
						    <p><a href="/preview/?d=mobile&l=/bhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template/" target="_blank" title="Demo of Download Mobile web template Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template">Demo</a> |
							<a href="/?l=8387&t=mobile" title="Download Mobile Version web template Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template" rel="nofollow">Download</a>
						<!--  /download/?l=/bhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template/&t=mobile---></p>
					 </div>
				   </div>		
				</div>
					</div>
		 <div class="post_description">
		 	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?32543a"></script>
<!-- w3layouts_main_790x90 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-9153409599391170"
     data-ad-slot="5168336680"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
	 <div class="download-buttons  bg-blue">
		 	
		 	<div class="download_button donate_button">
				   	   <p>Download</p>
				   	   	  <a href="/?donate=8387">Donate Remove Back Links</a>
				   	   	  <div class="clear"></div>
		   </div>
		 	<ul>
		 		<li><a href="/?l=8387&t=web" title="Download web version Free website template CSS HTML5 Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template" rel="nofollow"> Web Version</a></li>
		 		<li><a href="/?l=8387&t=smartphone" title="Download Iphone Smratphone Version web template Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template" rel="nofollow"> Smartphone Version</a></li>
		 		<li><a href="/?l=8387&t=mobile" title="Download Mobile Version web template Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template" rel="nofollow"> Mobile Version</a></li>
		 		<li><a href="/?l=8387&t=pack" title="Free Web and Mobile Website Template Set" rel="nofollow">Download Pack &nbsp;<div class="download_digits">(1,114)</div></a></li>
		 		<div class="clear"></div>
		 	</ul>
		</div>   
			  	 <div class="social-networks">
			  	 	<p>Share Template</p>
			  	 	<ul>
			  	 		<li>
			  	 			<a target="_blank" href="https://twitter.com/share?url=http%3A%2F%2Fw3l.in%2F%3Fp%3D8387&text=Bhagaskara+Free+%23Responsive+%23HTML5+%23CSS3+%23Mobileweb+Template++"><div class="networks_img twitter"><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/social/twitter.jpg?32543a" alt="Twit about this template and Share" /></div><!--<div class="count_numbers"></div>-->
			  	 			<div class="clear"></div>
			  	 			</a>
			  	 	    </li>
			  	 	    <li>
			  	 			<a target="_blank" href="https://www.facebook.com/dialog/feed?app_id=205270542940369&link=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&picture=http%3A%2F%2Fw3layouts.com%2Fwp-content%2Fuploads%2F2014%2F11%2FBhagaskara-future.jpg&name=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template&redirect_uri=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F" target="_blank"><div class="networks_img facebook"><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/social/facebook.jpg?32543a" alt="Like this Template at Facebook" /></div><!--<div class="count_numbers"></div>--->
			  	 			<div class="clear"></div>
			  	 			</a>
			  	 	    </li>
			  	 	    
			  	 	    <li>
			  	 			<a target="_blank" href="http://pinterest.com/pin/create/button/?url=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&media=http%3A%2F%2Fw3layouts.com%2Fwp-content%2Fuploads%2F2014%2F11%2FBhagaskara-web.jpg&description=Bhagaskara+Free+%23Responsive+%23HTML5+%23CSS3+%23Mobileweb+Template++"><div class="networks_img pinit"><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/social/pinit.jpg?32543a" alt="Pin this Template" /></div><!--<div class="count_numbers"></div>-->
			  	 			<div class="clear"></div>
			  	 			</a>
			  	 	    </li>
			  	 	    <li>
			  	 			<a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&title=Bhagaskara+Free+%23Responsive+%23HTML5+%23CSS3+%23Mobileweb+Template++&source=w3layouts.com"><div class="networks_img likedin"><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/social/likedin.jpg?32543a" alt="Share this Template with your Linkedin network" /></div><!--<div class="count_numbers"></div>--->
			  	 			<div class="clear"></div>
			  	 			</a>
			  	 	    </li>
			  	 	    <li>
			  	 			<a target="_blank" href="https://plus.google.com/share?url=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F"><div class="networks_img googleplus"><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/social/googleplus.jpg?32543a" alt="Share this Templates at Google+" /></div><!--<div class="count_numbers"></div>-->
			  	 			<div class="clear"></div>
			  	 			</a>
			  	 	    </li>
			  	 	    <li>
			  	 			<a target="_blank" href="http://technorati.com/faves?add=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template"><div class="networks_img techno"><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/social/technorati.jpg?32543a" alt="Share this Template at Technorati" /></div><!--<div class="count_numbers">0</div>-->
			  	 			<div class="clear"></div>
			  	 			</a>
			  	 	    </li>
			  	 	    <li>
			  	 			<a target="_blank" href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template">
			  	 				<div class="networks_img techno"><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/social/stumbleupon.jpg?32543a" alt="Stumble this Template" /></div><!--<div class="count_numbers">0</div>-->
			  	 			<div class="clear"></div>
			  	 			</a>
			  	 	    </li>
			  	 	    <li>
			  	 			<a target="_blank" href="http://www.tumblr.com/share?v=3&u=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&t=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template">
			  	 				<div class="networks_img techno"><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/social/tumblr.jpg?32543a" alt="Share Template at Tumbler" /></div><!--<div class="count_numbers">0</div>-->
			  	 			<div class="clear"></div>
			  	 			</a>
			  	 	    </li>
			  	 	    <li>
			  	 			<a target="_blank" href="http://www.evernote.com/clip.action?url=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template">
			  	 				<div class="networks_img techno"><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/social/evernote.jpg?32543a" alt="Share Template at Tumbler" /></div><!--<div class="count_numbers">0</div>-->
			  	 			<div class="clear"></div>
			  	 			</a>
			  	 	    </li>
			  	 	        <li>
			  	 			<a target="_blank" href="http://www.reddit.com/submit?url=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template">
			  	 				<div class="networks_img techno"><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/social/reddit.jpg?32543a" alt="Share Template at reddit" /></div><!--<div class="count_numbers">0</div>-->
			  	 			<div class="clear"></div>
			  	 			</a>
			  	 	    </li>
			  	 	     <li>
			  	 			<a target="_blank" href="http://del.icio.us/post?url=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template&notes=Bhagaskara+Free+%23Responsive+%23HTML5+%23CSS3+%23Mobileweb+Template++">
			  	 				<div class="networks_img techno"><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/social/delicious.jpg?32543a" alt="Share Template at delicious" /></div><!--<div class="count_numbers">0</div>-->
			  	 			<div class="clear"></div>
			  	 			</a>
			  	 	    </li>
			  	 	     <li>
			  	 			<a target="_blank" href="http://www.friendfeed.com/share?url=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template">
			  	 				<div class="networks_img techno"><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/social/friendfeed.jpg?32543a" alt="Feed Template at friendfeed" /></div><!--<div class="count_numbers">0</div>-->
			  	 			<div class="clear"></div>
			  	 			</a>
			  	 	    </li>
			  	 	      <li>
			  	 			<a target="_blank" href="http://ping.fm/ref/?link=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template&body=Bhagaskara+Free+%23Responsive+%23HTML5+%23CSS3+%23Mobileweb+Template++">
			  	 				<div class="networks_img techno"><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/social/pingfm.jpg?32543a" alt="Ping Template at ping.fm" /></div><!--<div class="count_numbers">0</div>-->
			  	 			<div class="clear"></div>
			  	 			</a>
			  	 	    </li>
			  	 	         <li>
			  	 			<a target="_blank" href="http://www.newsvine.com/_tools/seed&save?u=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&h=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template">
			  	 				<div class="networks_img techno"><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/social/newsvine.jpg?32543a" alt="Publish Template at newsvine" /></div><!--<div class="count_numbers">0</div>-->
			  	 			<div class="clear"></div>
			  	 			</a>
			  	 	    </li>
			  	 	   <!--- http://www.reddit.com/submit?url=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template			  	 	    http://del.icio.us/post?url=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template&notes=Bhagaskara+Free+%23Responsive+%23HTML5+%23CSS3+%23Mobileweb+Template++			  	 	    http://tapiture.com/bookmarklet/image?img_src=http%3A%2F%2Fw3layouts.com%2Fwp-content%2Fuploads%2F2014%2F11%2FBhagaskara-web.jpg&page_url=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&page_title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template&img_title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template			  	 	    http://www.stumbleupon.com/submit?url=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template			  	 	    http://technorati.com/faves?add=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template			  	 	    http://www.tumblr.com/share?v=3&u=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&t=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template			  	 	    http://www.friendfeed.com/share?url=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template			  	 	    http://www.evernote.com/clip.action?url=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template			  	 	    http://ping.fm/ref/?link=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template&body=Bhagaskara+Free+%23Responsive+%23HTML5+%23CSS3+%23Mobileweb+Template++			  	 	    http://www.newsvine.com/_tools/seed&save?u=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&h=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template	
			  	 	    http://posterous.com/share?linkto=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F			  	 	    http://slashdot.org/bookmark.pl?url=http%3A%2F%2Fw3layouts.com%2Fbhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template%2F&title=Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template			  	 	---></ul>
			  	 </div>
			  	 
			   <!--<div class="download_pack">
			   	
			   	   <div class="download_button">
			   	   	  <a href="/?l=8387&t=pack" title="Free Web and Mobile Website Template Set" rel="nofollow"><i><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/download-icon.png?32543a"></i> Download Pack &nbsp;<div class="download_digits">(1,114)</div></a>
			   	  <!-- /download/?l=/bhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template/&t=pack--->
            <!--  <div class="download_button" style="padding-top: 1px">
				   	   	  <a href="/?donate=8387">Donate Remove Back Links</a>
				   	   </div>
				   	  </div>--->
              
				   	   	
	               </div>
			   <div class="clear"></div>
	<article id="post-8387" class="post-8387 post type-post status-publish format-standard has-post-thumbnail hentry category-corporate-web-templates tag-bootstrap tag-bootstrap-templates tag-corporate tag-corporate-business tag-corporate-portfolio tag-flat tag-flat-style tag-multipurpose tag-singlepage">
		<div class="post_data">
		<p>

		<h2>Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template</h2>
<p>Bhagaskara is a clean, flat and professional Multipurpose Template for agencies and creative studios. It can be customized easily to suit your wishes.<br />
That comes with a free Flat Responsive web design template. You can use this template for any type of websites. This web template is built in a Fancy style however it can be used  as per the user requirements. Bhagaskara designed with a clean flat grid system. These are some strong points to consider if you plan to use this free web design template.</p>
<p>Bhagaskara Multipurpose Flat PSD by <strong>Andreansyah Setiawan&#8217;s</strong></p>
<p>Download PSD : <a href="https://www.behance.net/gallery/16872707/Bhagaskara-Onepage-PSD-Template" target="_blank">Here</a></p>
			</p>

		<footer>
			<p>This entry was posted in <a href="http://w3layouts.com/corporate-web-templates/" rel="category tag">Corporate Business</a> and tagged <a href="http://w3layouts.com/tag/bootstrap/" rel="tag">Bootstrap</a>, <a href="http://w3layouts.com/tag/bootstrap-templates/" rel="tag">Bootstrap Templates</a>, <a href="http://w3layouts.com/tag/corporate/" rel="tag">Corporate</a>, <a href="http://w3layouts.com/tag/corporate-business/" rel="tag">Corporate Business</a>, <a href="http://w3layouts.com/tag/corporate-portfolio/" rel="tag">Corporate Portfolio</a>, <a href="http://w3layouts.com/tag/flat/" rel="tag">Flat</a>, <a href="http://w3layouts.com/tag/flat-style/" rel="tag">Flat Style</a>, <a href="http://w3layouts.com/tag/multipurpose/" rel="tag">Multipurpose</a>, <a href="http://w3layouts.com/tag/singlepage/" rel="tag">singlepage</a>. Bookmark the <a href="http://w3layouts.com/bhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template/" title="Permalink to Bhagaskara a onepage Multipurpose Flat Bootstrap Responsive web template" rel="bookmark">permalink</a>. Posted on <a href="http://w3layouts.com/bhagaskara-onepage-multipurpose-flat-bootstrap-responsive-web-template/" title="4:49 PM" rel="bookmark"><time class="entry-date" datetime="2014-11-06T16:49:47+00:00">November 6, 2014</time></a><span class="byline"> by <span class="author vcard"><a class="url fn n" href="http://w3layouts.com/author/w3layouts/" title="View all posts by w3layouts" rel="author">w3layouts</a></span></span></p>
			<p></p>
			<p></p>
		</footer>
		</div>
</article>
			
			
							
</div>	
</div>
<div class="sidebar">
	<div class="add">
    	    	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?32543a"></script>
<!-- w3layouts -->
<ins class="adsbygoogle" style="display:block;width:300px;height:250px;margin: 0 auto;" data-ad-client="ca-pub-9153409599391170" data-ad-slot="9492948281"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
    	    	</div>	
				  <h1>Related Templates</h1>			
    <div class="latest_template">
				  	   <a href="http://w3layouts.com/ket-singlepage-multipurpose-flat-bootstrap-responsive-web-template/"><img src="http://wwwcdn.w3layouts.com/wp-content/uploads/2014/11/ket-future-300x190.jpg?32543a"  alt="Ket a Singlepage Multipurpose Flat Bootstrap Responsive web template Mobile website template Free" />
					   <h2>Ket</h2></a>
				  </div>
    <div class="latest_template">
				  	   <a href="http://w3layouts.com/orion-corporate-portfolio-flat-bootstrap-responsive-web-template/"><img src="http://wwwcdn.w3layouts.com/wp-content/uploads/2014/11/orian-future-300x190.jpg?32543a"  alt="Orion a Corporate Portfolio Flat Bootstrap Responsive web template Mobile website template Free" />
					   <h2>Orion</h2></a>
				  </div>
    <div class="latest_template">
				  	   <a href="http://w3layouts.com/gerdu-kreatip-corporate-multipurpose-flat-bootstrap-responsive-web-template/"><img src="http://wwwcdn.w3layouts.com/wp-content/uploads/2014/11/gk-future-300x190.jpg?32543a"  alt="Gerdu Kreatip a Corporate Multipurpose Flat Bootstrap Responsive web template Mobile website template Free" />
					   <h2>Gerdu Kreatip</h2></a>
				  </div>
    <div class="latest_template">
				  	   <a href="http://w3layouts.com/arrow-corporate-portfolio-flat-bootstrap-responsive-web-template/"><img src="http://wwwcdn.w3layouts.com/wp-content/uploads/2014/10/Arrow-future-300x190.jpg?32543a"  alt="Arrow a Corporate Portfolio Flat Bootstrap Responsive web template Mobile website template Free" />
					   <h2>Arrow</h2></a>
				  </div>
    <div class="latest_template">
				  	   <a href="http://w3layouts.com/dayoh-landingpage-multipurpose-flat-bootstrap-responsive-web-template/"><img src="http://wwwcdn.w3layouts.com/wp-content/uploads/2014/10/dayoh-future-300x190.jpg?32543a"  alt="Dayoh a landingpage Multipurpose Flat Bootstrap Responsive web template Mobile website template Free" />
					   <h2>Dayoh</h2></a>
				  </div>
              <div class="add">
    	    		<!--<div id="bsap_1276874" class="bsarocks bsap_732485bcf97a6076cf6de4b036204fdf"></div>
    	    	 BuySellAds Zone Code -->
<div id="bsap_1292875" class="bsarocks bsap_732485bcf97a6076cf6de4b036204fdf"></div>
<!-- End BuySellAds Zone Code -->
</div>    	    	
</div>
<div class="clear"></div>
</div>


	</div><!-- #main -->
<div class="footer">
    	<div class="wrap">
    		<div class="foot-logo"><img src="http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/images/web/foot-logo.png?32543a"/></div>
    		<div class="foot-links">
    			<ul>
    				<h3>Links</h3>
    				<li><a href="http://w3layouts.com/">Home</a></li>
    				<li><a href="http://w3layouts.com/about">About</a></li>
    				<li><a href="http://w3layouts.com/faqs">FAQ</li></a>
    				<li><a href="http://support.w3layouts.com">Catch Us</a></li>
    			</ul>
    		<ul>
    				<h3>Others</h3>
    				<li><a href="http://w3layouts.com/privacy-policy/">Privacy Policy</a></li>
    				<li><a href="http://support.w3layouts.com">Support</a></li>
    		</ul>
    			<ul class="social">
    				<h3>Catch on Social</h3>
    				<li><a href="http://www.facebook.com/w3layouts">Facebook  - <span>49015+</span></a></li>
    				<li><a href="http://twitter.com/w3layouts">Twitter - <span>14330+</span></a></li>
    				<li><a href="https://plus.google.com/+w3layouts/?rel=author">Google+ - <span>575+</span></li></a>
    			</ul>
    			<div class="clear"></div>
    		</div>
    		<div class="clear"></div>
    	</div>
    </div>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-30027142-1', 'w3layouts.com');
  ga('send', 'pageview');

</script>
 <script type="text/javascript">
				$(document).ready(function() {			
					$().UItoTop({ easingType: 'easeOutQuart' });
					
				});
			</script>
		    <a href="#" id="toTop"><span id="toTopHover"> </span></a>
		      
<!-- Start Alexa Certify Javascript -->
 	<script type="text/javascript">
_atrk_opts = { atrk_acct:"HHmdj1aAkN00GK", domain:"w3layouts.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=HHmdj1aAkN00GK" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->  
<script type='text/javascript' src='http://wwwcdn.w3layouts.com/wp-content/themes/w3layouts/js/navigation.js?32543a?ver=20120206'></script>
</body>
</html>