

# wget $1 --cookies=on --keep-session-cookies --save-cookies=cookie.txt --quiet
agent="Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36"

if [[ -z "$3'" ]]; then
	echo "sh dl.sh $1(url) $2(folder) $3(*zip, filename)"
	exit
fi
dl="$1"
tofolder="$2"
filename="$3"
echo -ne "\033[33m $dl download start...      \033[0m"
wget -O "$tofolder/$3" $dl  \
--referer='$reffer' \
--user-agent='$agent' \
--progress=bar \

