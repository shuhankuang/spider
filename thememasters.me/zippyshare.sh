#!/bin/bash
# @Description: zippyshare.com file download script
# @Author: Live2x
# @URL: live2x.com
# @Version: 1.0.20141117
# @Date: 2014/11/17
# @Usage: sh zippyshare.sh filename path


if [ -z "$1" ]; then
    echo usage: $0 filename
    exit
fi

echo "Goto Zippyshare and Start Download..."

if [ -f "$1" ]; then
  rm -rf $1
fi

if [ -f "cookie.txt" ]; then
  rm -rf cookie.txt
fi

wget -O info.txt $1 --cookies=on --keep-session-cookies --save-cookies=cookie.txt --quiet



if [ -f "cookie.txt" ]; then 
    jsessionid=`cat cookie.txt | grep "JSESSIONID" | cut -f7`
    #echo "JSESSIONID="$jsessionid
else
    echo "can't find cookie file"
    exit
fi

echo "Goto Zippyshare and Start Download..."


if [ -f "info.txt" ]; then
    # document.getElementById('dlbutton').href = "/d/80490089/" + (541541 % 51245 + 541541 % 913) + "/userrole417.rar";
    #a=`cat info.txt | grep "var a =" | cut -d'=' -f2 | cut -d';' -f1 | grep -o "[^ ]\+\(\+[^ ]\+\)*"`
    x1=`cat info.txt | grep "getElementById('dlbutton').href" | cut -d'+' -f1 | cut -d'"' -f2`
    # echo $x1
    a=`cat info.txt | grep "getElementById('dlbutton').href" | cut -d'+' -f2 | cut -d'(' -f2 | cut -d'%' -f1`
    b=`cat info.txt | grep "getElementById('dlbutton').href" | cut -d'+' -f2 | cut -d'(' -f2 | cut -d'%' -f2`
    # echo $a $b
    c=`cat info.txt | grep "getElementById('dlbutton').href" | cut -d'+' -f3 | cut -d')' -f1 | cut -d'%' -f1`
    d=`cat info.txt | grep "getElementById('dlbutton').href" | cut -d'+' -f3 | cut -d')' -f1 | cut -d'%' -f2`
    # echo $c $d
    #echo "a="$x1%$x2

    filename=`cat info.txt | grep "property=\"og:title\"" | cut -d'"' -f4 | grep -o "[^ ]\+\(\+[^ ]\+\)*"`
    # filename=`cat info.txt | grep "b+18" | head -n 1 | cut -d'/' -f5 | cut -d'"' -f1`
    # echo "filename="$filename
    # exit
    # if [ -z $filename ]; then
    #   filename=`cat info.txt | grep "/d/" | cut -d'/' -f5 | cut -d'"' -f1`
    # fi
    
    reffer=`cat info.txt | grep "property=\"og:url\"" | cut -d'"' -f4 | grep -o "[^ ]\+\(\+[^ ]\+\)*"`
    #echo "reffer="$reffer

    server=`echo "$reffer" | cut -d'/' -f3`
    # echo "server="$server

    # id=`echo "$reffer" | cut -d'/' -f5`
    # echo "id="$id
else
    echo "can't find info file"
    exit
fi

# exit


zz=$[(a % b) + (c % d)]

dl="http://"$server""$x1""$zz"/"$filename
# echo $dl
#

agent="Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36"

echo -ne "\033[33m $filename download start...      \033[0m"
wget -c -O "$2"$filename $dl \
--referer='$reffer' \
--cookies=off --header "Cookie: JSESSIONID=$jsessionid" \
--user-agent='$agent' \
--progress=bar \

rm -rf cookie.txt
rm -rf info.txt

if [ -s "$2"$filename ]; then
    echo -e "\033[32m Download success! \033[0m"
else
    rm -rf ${filename}
    echo -e "\033[31m Download error! \033[0m"
fi