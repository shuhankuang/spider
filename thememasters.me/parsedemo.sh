
# $1 _tf $2 _d_tf $3 folder
function _parseDemo() {


	# 旧规则
	# themeforest.net  codecanyon.net
	_old_tf=`cat "$page" | grep 'Demo:' | grep 'themeforest.net'`
	_old_cy=`cat "$page" | grep 'Demo:' | grep 'codecanyon.net'`
	_old_gr=`cat "$page" | grep 'Demo:' | grep 'graphicriver.net'`

	# 新规则
	# themeforest.net  codecanyon.net
	_tf=`cat "$page" | grep 'themeforest.net'`
	_cy=`cat "$page" | grep 'codecanyon.net'`
	_gr=`cat "$page" | grep 'graphicriver.net'`

	__tf=`cat "$page" | grep 'themeforest.net' | grep 'Demo:'`
	__cy=`cat "$page" | grep 'codecanyon.net' | grep 'Demo:'`
	__gr=`cat "$page" | grep 'graphicriver.net' | grep 'Demo:'`


	_prase "$_tf"
	_prase "$_cy"
	_prase "$_gr"
	
}

_prase() {

	a=$1
	isCy="codecanyon"
	isTf="themeforest"
	isGr="graphicriver"

	strindex() { 
	  x="${1%%$2*}"
	  [[ $x = $1 ]] && echo -1 || echo ${#x}
	}

	if [[ $(strindex "$a" "$isCy") != -1 ]]; then
		index=$(strindex "$a" "$isCy")
		demo="http:/"`echo "$a" | cut -b "$index"-2000 | cut -d'<' -f1`
	fi

	if [[ $(strindex "$a" "$isTf") != -1 ]]; then
		index=$(strindex "$a" "$isTf")
		demo="http:/"`echo "$a" | cut -b "$index"-2000 | cut -d'<' -f1`
	fi

	if [[ $(strindex "$a" "$isGr") != -1 ]]; then
		index=$(strindex "$a" "$isGr")
		demo="http:/"`echo "$a" | cut -b "$index"-2000 | cut -d'<' -f1`
	fi

	# demo=`echo "$a" | cut -b `
	if [[ ! -z "$demo" ]]; then
		_save "$demo"
	fi
}

function _save() {
	demo=$1
	if [[ ! -z "$demo" ]]; then
		echo "_save ""$demo"
		echo "_save to ""$folderName"
		echo "$demo" > demo.txt
		printf "%s" "`cat demo.txt`" > demo.txt
		mv demo.txt "$folderName/"demo.txt
		# echo|set /p="$demo" > "$folderName/"demo.txt
		# printf "%s" "`cat "$folderName"/demo.txt`" >> "$folderName/"demo.txt
		# 
		# 下载预览图
		sh downloadpreview.sh "$demo" "$folderName"
	fi
	
}



a='<p style="font-family: Verdana; font-size: 10.9090909957886px; line-height: 16.5000019073486px; word-spacing: 1.10000002384186px;"><div align="center"><br />Demo:<br /><span style="font-size: 10.9090909957886px; line-height: 16.5000019073486px; word-spacing: 1.10000002384186px;">http://themeforest.net/item/lawbusiness-attorney-lawyer-wordpress-theme/7581460</span></p>'
b='<p>http://themeforest.net/item/ink-a-wordpress-blogging-theme-to-tell-stories/7520750</p>'


echo "Start Parse Demo..." $1 $2
page=$1
folderName=$2

_parseDemo "$page" "$folderName"
