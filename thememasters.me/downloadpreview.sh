# 

# arguments $1 tf路径 $2 存放路径
tf=$1
# tf='http://codecanyon.net/item/crocdesk-help-desk-solution/5636027?WT.ac=weekly_feature&WT.z_author=jakweb'
path=$2

echo "Goto Envato Download Preview Image"

if [[ $tf ]]; then
	wget -O "$path/tf.txt" $tf --cookies=on --keep-session-cookies --quiet
fi

if [[ -f "$path/tf.txt" ]]; then
	# 按条件进行下载预览图
	if [[ $tf == *themeforest* ]]; then
		preview=`cat "$path/tf.txt" | grep 'target="_blank"><img alt=' | cut -d'"' -f10`
	fi

	if [[ $tf == *codecanyon* ]]; then
		preview=`cat "$path/tf.txt" | grep 'target="_blank"><img alt=' | cut -d'"' -f10`
	fi

	if [[ $tf == *graphicriver* ]]; then
		preview=`cat "$path/tf.txt" | grep 'itemprop="image"' | cut -d'"' -f6`
	fi

	wget "$preview" -O "$path/preview.png"  --quiet

	echo "$preview" > preview.txt
	printf "%s" "`cat preview.txt`" > preview.txt
	mv preview.txt "$path/"preview.txt


	# echo|set /p=$preview > "$path/"preview.txt
	# printf "%s" "`cat "$path"/preview.txt`" > "$path/"preview.txt
	echo "Finish Download Preview Image and Exit TF"
fi