#!/bin/bash
#

strindex() { 
  x="${1%%$2*}"
  [[ $x = $1 ]] && echo -1 || echo ${#x}
}

# $1 <h3 class="btl"><a href="http://www.thememasters.me/wordpress-themes/9312-jupiter-v404-multi-purpose-responsive-theme.html">Jupiter v.4.0.4 - Multi-Purpose Responsive Theme</a></h3>

template=$1

if [[ ! $template ]]; then
	#statements
	echo '没有指定模板下载地址'
	exit
fi

if [[ ! -f "download" ]]; then
	mkdir -p "download"
fi


# 文件名及模板的详细页面地址
folderName=`echo "$template" | cut -d'>' -f3 | cut -d'<' -f1`

ts=`cat "ts.txt" | grep "$folderName"`

if [[ ! -z "$ts" ]]; then
	echo ":: 此文件已经下载($ts)"
	exit
fi

mkdir -p "download/""$folderName"

echo $folderName > "download/""$folderName/"name.txt
printf "%s" "`cat "download/""$folderName/"name.txt`" > "download/""$folderName/"name.txt

echo "Start Download... "$folderName


#放到download下
folderName="download/""$folderName"
detailLink=`echo "$template" | cut -d'"' -f4`

# 保存此模板的链接地址
echo $detailLink > "$folderName"/link.txt

# 备用解析
page="$folderName""/page.txt"

if [[ ! -f "$page" ]]; then
	wget -O "$page" "$detailLink" --cookies=on --keep-session-cookies --quiet
fi


if [[ -f "$page" ]]; then
	# demo for online preview
 
	

	sh parsedemo.sh "$page" "$folderName"
	

	# demo=`cat "$page" | grep 'Demo:' | cut -d'>' -f6 | cut -d'<' -f1`
	# if [[ -z "$demo" ]]; then
	# 	echo "@ Empty _Demo_ "$folderName
	# 	demo=""
	# fi
	# echo|set /p=$demo > "$folderName/"demo.txt
	# printf "%s" "`cat "$folderName"/demo.txt`" >> "$folderName/"demo.txt

	# category
	category=`cat "$page" | grep 'Category: ' | cut -d'>' -f4 | cut -d'<' -f1`
	if [[ -z "$category" ]]; then
		echo "@ Empty _Category_ "$folderName
		category=""
	fi
	echo "$category" > "$folderName/"category.txt
	printf "%s" "`cat "$folderName"/category.txt`" > "$folderName/"category.txt

	#zippyshare
	zippyshare=`cat "$page" | grep 'zippyshare.com'`

	# if get the zippyshare, goto and download
	if [[ ! -z "$zippyshare" ]]; then

		index=$(strindex "$zippyshare" "http://")
		zippyshare=`echo "$zippyshare" | cut -b "$index"-2000 | cut -d'<' -f1 | cut -d'>' -f2`

		echo "zip ""$zippyshare"

		echo "$zippyshare" > "$folderName/"zippyshare.txt
		printf "%s" "`cat "$folderName"/zippyshare.txt`" > "$folderName/"zippyshare.txt
		sh zippyshare.sh "$zippyshare" "$folderName/"
	fi
	
	
fi

