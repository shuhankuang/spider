#!/bin/bash
#
#

ts=`ls -t download/`
echo "$ts" > ts.txt

if [[ -z "$1" ]]; then
	echo "Msg: 请用 sh run.sh 1 10 , 第一个参数是页码，第二个参数是到那页" 
	exit
fi


# log start
DATE=`date +%Y-%m-%d:%H:%M:%S`
log="$DATE"" from "$1" to "$2
echo "$log" >> log.txt

for (( i = "$2"; i >= "$1"; i-- )); do
	sh go.sh $i
done

# log complete
DATE=`date +%Y-%m-%d:%H:%M:%S`
echo "$DATE"" complete" >> log.txt

rm ts.txt
# sh sync.sh

# 查看运行进程信息
# ps auxww
# ps auxww | grep sh 能看到运行到第几页