#!/bin/bash
#
if [[ ! $1 ]]; then
    echo '请输入过滤的日期关键字， 如 lt -lt | grep "Jan  4"，注意空格'
    exit
fi

if [[ -f 'json.json' ]]; then
    rm json.json
fi

path="download"
a=`ls -tl "$path" | grep "Jan  4"`

echo "$a" > templates.txt

#remove empty line on the end
printf "%s" "`cat templates.txt`" > _templates.txt


# exit
json="["
# parse templates to download
if [[ -f 'templates.txt' ]]; then
	i=1
	sum=`sed -n '$=' templates.txt`
	while read line
    do
        arr[$i]="$line"
        i=`expr $i + 1`
    done < templates.txt

    t=$[sum]
    i=1

    for i in `seq $sum` ;do

    	folder="$path/"`echo ${arr[i]} | cut -b 42-2000`
        echo "$folder"
    	if [[ -d "$folder" ]]; then

    	    if [[ -f "$folder/link.txt" ]]; then
                echo $i
    	    	category=`cat "$folder/category.txt"`
    	    	title_en=`cat "$folder/name.txt"`
    	    	# link=`cat "$folder/link.txt"`
    	    	previewURL=`cat "$folder/demo.txt"`
    	    	thumbnail=`cat "$folder/preview.txt"`
                downloadURL=""
                originalURL=`cat "$folder/link.txt"`

                # 下载的内容
                zip=`ls "$folder" | grep '.zip'`
                rar=`ls "$folder" | grep '.rar'`
                if [[ ! -z "$zip" ]]; then
                    downloadURL="$zip"
                fi
                if [[ ! -z "$rar" ]]; then
                    downloadURL="$rar"
                fi
                # 
    	    	
    	    	obj="{\"category\":\"$category"\",\"title_en\":\""$title_en""\",\"thumbnail\":\""$thumbnail"\",\"previewURL\":\""$previewURL"\",\"downloadURL\":\""$downloadURL"\",\"originalURL\":\""$originalURL"\"}"
                echo $i
    	    	if [[ $i -lt $t ]]; then
                    echo $i $t
    	    		obj="$obj,"
    	    	fi
    	    	json="$json""$obj"
    	    fi

        elif [[ ! -d "$folder" ]]; then
            echo "Not Found Folder!"
    	fi

	    if [[ $i -eq $sum ]]; then
	      	echo 'Generate JSON Success!'
	      	# exit 0
	    fi

    done
fi

json="$json]"
echo "$json" > json.json

rm templates.txt
rm _templates.txt
