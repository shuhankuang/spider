#!/bin/bash
# arguments: $1 page
website="http://www.thememasters.me/"
page=$1

# start
if [[ -z $page ]]; then
	echo ": 请输入需要抓取的页码，如 sh go.sh 1"
	exit
fi

echo "Goto Page: "$1

if [[ ! -f 'page.txt' ]]; then
	_w=$website;

	if [[ $page > 1 ]]; then
		_w=$website'page/'$page
	fi

	wget -O page.txt $_w --cookies=on --keep-session-cookies --quiet
fi

# parse page to get templates link
if [[ -f 'page.txt' ]]; then
	templates=`cat page.txt | grep '<h3 class="btl">'`
	# echo "$templates"
	echo "$templates" > templates.txt
fi
# parse templates to download
if [[ -f 'templates.txt' ]]; then
	i=1
	sum=`sed -n '$=' templates.txt`
	while read line
    do
        arr[$i]="$line"
        i=`expr $i + 1`
    done < templates.txt

    i=1
    for i in `seq $sum` ;do 
    	# echo 'download' ${arr[i]}
      sh download.sh "${arr[i]}"
      # echo ${arr[i]}
      if [[ $i -eq $sum ]]; then
      	 echo 'done'
      	 # exit 0
      fi

    done

fi

rm page.txt
rm templates.txt

